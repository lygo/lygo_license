module bitbucket.org/lygo/lygo_license

go 1.14

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_events v0.1.10
	github.com/google/uuid v1.3.0 // indirect
)
