# LyGo License #

![image](icon.png)

Simple license manager that ask a remote server for a license file 
to check



## Dependencies ##

`go get -u bitbucket.org/lygo/lygo_commons`

`go get -u bitbucket.org/lygo/lygo_events`

## How to Use ##

To use just call:

```
go get -u bitbucket.org/lygo/lygo_license@v0.1.3
```

### Versioning ###

Sources are versioned using git tags:

```
git tag v0.1.3
git push origin v0.1.3
```


